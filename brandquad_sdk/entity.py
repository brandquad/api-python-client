import json
from json import JSONDecodeError

import requests

from brandquad_sdk.helpers import AttributeDict, SlicableOrderedDict


class Entity(object):
    def __init__(self, data=None, name=None, entry_point=None, help_fields=None):
        self._data = data
        self._name = name
        self._entry_point = entry_point
        self._path = entry_point._path
        self._api = entry_point._api
        self._attributes = []
        self.help_fields = help_fields
        self.error = None
        self.status_code = None

        self._name_mapping = {'File': ['links'],
                              'Folder': ['files'],
                              'Product': ['categories', 'relations', 'assets', 'set']}
        self._initial()

        super(Entity, self).__init__()
        self.changed = False

    def _initial(self):
        if self._data is None:
            if self._name != 'File':
                try:
                    self._attributes = [key for key in self.help_fields]
                except TypeError:
                    pass
            else:
                self._attributes = ['url', 'file', 'folder']
        else:
            self._attributes = self._data.keys()

        for attr in self._attributes:
            if attr == 'attributes':
                self._data[attr] = AttributeDict(self._change, self._data[attr])
            setattr(self, attr, self._data[attr] if self._data else None)
            if attr == 'meta':
                setattr(self, 'id', self._data[attr]['id'])

        if hasattr(self, 'id') and self.id and self._name and self._name_mapping.get(self._name):
            for attr in self._name_mapping[self._name]:
                initial_attr = None
                if hasattr(self, attr):
                    initial_attr = getattr(self, attr)
                    delattr(self, attr)

                setattr(self, attr, EntryPointFactory(self._entry_point._api,
                                                      '{}%s/{}/'.format(self._entry_point._path, attr),
                                                      initial_data=initial_attr,
                                                      entity_pk=self.id))

    def __str__(self):
        obj_id = self.id if hasattr(self, 'id') else self.meta['id'] if hasattr(self, 'meta') else None
        string = '<{} {}>'.format(self._name, obj_id) if obj_id else '<{} new>'.format(self._name)
        return string

    def __name__(self):
        return self._name

    def _change(self):
        self.changed = True

    def __setattr__(self, key, value):
        if hasattr(self, '_attributes') and hasattr(self, 'changed'):
            if not self.changed and key in self._attributes:
                self._change()
        super(Entity, self).__setattr__(key, value)

    def _collect_data(self):
        for attr in self._attributes:
            attr_data = getattr(self, attr)
            if self._data is None:
                self._data = {}
            if isinstance(attr_data, Entity):
                attr_data = attr_data.id if hasattr(attr_data, 'id') else attr_data.meta['id']
            self._data.update({attr: attr_data}) if attr_data else None
        return self._data

    def save(self):
        """
        Perform save existing object, or create, if object does not exist on server
        :return: saved object
        """
        data = self._collect_data()

        if self._name == 'Product':

            if hasattr(self, 'attributes'):
                data = self.attributes
            elif hasattr(self, 'data'):
                data = self.data
                delattr(self, 'data')
            else:
                data = {}

            data = {'data': json.dumps(data)}

        if hasattr(self, 'id') and self.id:
            response = self._api.get_response(requests.patch, data=data, path=self._path, pk=self.id)
        else:
            response = self._api.get_response(requests.post, data=data, path=self._path)

        status = int(response.status_code/100)

        if status != 5:
            _data = json.loads(response.text)
            if status != 4:
                self._data = _data[0] if isinstance(_data, list) else _data
            else:
                self.error = _data
        else:
            self.error = response.text

        self.status_code = response.status_code

        self._initial()
        self._entry_point._update_object(self)
        return self

    def delete(self, leave_in_cache=False):
        """
        Delete object from server
        :param leave_in_cache: if True leaving object in cache in entrypoint-object in entrypoint.items
        :return: None if item not in cache
        """
        if hasattr(self, 'id'):
            response = self._api.get_response(requests.delete, path=self._path, pk=self.id)
            self.status_code = response.status_code
            self._entry_point.status_code = self.status_code

            status = int(response.status_code / 100)

            if not leave_in_cache and status != 5 and status != 4:
                self._entry_point.items.pop(self.id)

            if self.id in self._entry_point.items:
                return self
            else:
                return None

    def dict(self):
        """
        :return: dict of object's attributes
        """
        return self._collect_data()

    __repr__ = __str__


class EntryPointFactory(object):
    def __init__(self, api, path, initial_data=None, entity_pk=None):
        entity_mapping = {'attributes/': ('Attribute', 'Attributes'),
                          'attributes/types/': ('Type', 'Types'),
                          'attributes/groups/': ('Group', 'Groups'),
                          'categories/': ('Category', 'Categories'),
                          'dam/files/': ('File', 'Files'),
                          'dam/folders/': ('Folder', 'Folders'),
                          'dam/folders/%s/files/': ('File', 'Files'),
                          'products/': ('Product', 'Products'),
                          'dam/files/%s/links/': ('FileLink', 'FileLinks'),
                          'products/%s/categories/': ('ProductCategory', 'ProductCategories'),
                          'products/%s/relations/': ('ProductRelation', 'ProductRelations'),
                          'products/%s/assets/': ('ProductAsset', 'ProductAssets'),
                          'products/%s/set/': ('ProductInSet', 'ProductsSet')}

        self._api = api

        if 'dam/folders/' in path and 'links/' in path:
            path = 'dam/files/%s/links/'

        self._path = path
        self._entity = entity_mapping[path][0]
        self._name = entity_mapping[path][1]

        if self._name == 'Attributes':
            for attr in ['types', 'groups']:
                path = '{}{}/'.format(self._path, attr)
                setattr(self, attr, EntryPointFactory(self._api, path))

        if entity_pk:
            self._path = self._path % entity_pk

        self._next = None
        self._prev = None

        self.count = None
        self.items = SlicableOrderedDict()
        self.iter_scope = 0

        self._iter_items = None

        self.error = None
        self.status_code = None
        self.maximum_items = None
        self._request_params = {}

        self.help_fields = self._api.help_fields[self._name]
        if not self.help_fields:
            self._api.help_fields[self._name] = self.options()

        if initial_data:
            self._build_from_initial(initial_data)

    def __iterate(self):
        self.iter_scope = 0

        if self.count is None:
            self.__build_objects(self._api.get_response(requests.get, data=self._request_params, path=self._path))
        if self.count:
            for self.iter_scope in range(self.count):
                if self.maximum_items and self.iter_scope == self.maximum_items:
                    break
                if len(self.items) == self.iter_scope:
                    self.__build_objects(self._api.get_response(requests.get, fullurl=self._next))

    def _build_from_initial(self, initial_data):

        def _build(data):
            obj_id = data.get('id') if isinstance(data, dict) else None
            if obj_id:
                obj = Entity(data=data, name=self._entity, entry_point=self, help_fields=self.help_fields)
                return obj_id, obj
            return None, None

        if isinstance(initial_data, dict):
            obj_id, obj = _build(initial_data)
            self.items.update({obj_id: obj})
        else:
            for data in initial_data:
                obj_id, obj = _build(data)
                self.items.update({obj_id: obj})

    def _update_object(self, obj):
        if hasattr(obj, 'id'):
            self.items.update({obj.id: obj})

        self.error = obj.error
        self.status_code = obj.status_code

    def __build_objects(self, response):
        self.status_code = response.status_code
        raw_data = json.loads(response.text)
        obj_id = None
        items = []
        if isinstance(raw_data, list) and len(raw_data) == 1:
            raw_data = raw_data[0]
        if 'results' in raw_data:
            self.count = raw_data['count']
            self._next = raw_data['next']
            self._prev = raw_data['previous']
            items = raw_data['results']
        elif 'meta' in raw_data:
            obj_id = raw_data['meta']['id']
        elif 'id' in raw_data:
            obj_id = raw_data['id']
        elif isinstance(raw_data, list) and len(raw_data) > 1:
            if 'id' in raw_data[0]:
                items = raw_data
        else:
            self.error = raw_data

        if obj_id:
            obj = Entity(data=raw_data, name=self._entity, entry_point=self, help_fields=self.help_fields)
            self.items.update({obj_id: obj})
        elif not self.error:
            if items:
                for item in items:
                    obj = Entity(data=item, name=self._entity, entry_point=self, help_fields=self.help_fields)
                    obj_id = obj.id if hasattr(obj, 'id') else obj.meta['id']
                    self.items.update({obj_id: obj})

    def _get_from_cache(self, pk):
        if pk not in self.items and isinstance(pk, int):
            pk = str(pk)
        elif pk not in self.items and isinstance(pk, str):
            try:
                pk = int(pk)
            except (ValueError, TypeError):
                pass
        item = self.items.get(pk)
        return item

    def get(self, pk, force=False, **kwargs):
        """
        Get one object by identifier
        :param pk: identifier
        :param force: if True then get object from server and rewrite cached-one with same identifier
        :return:
        """

        if force or not self._get_from_cache(pk):
            self.__build_objects(self._api.get_response(requests.get, data=kwargs, path=self._path, pk=pk))

        return self._get_from_cache(pk)

    def list(self, force=False, maximum_items=None, **kwargs):
        """
        Get list of objects
        :param force: if True then get object from server
        :param maximum_items: count of returning objects
        :param kwargs: addition params, like page, page_size, filters or any from official documentation
        :return: list of objects limited by maximum_items
        """
        self.maximum_items = maximum_items
        self._request_params = kwargs
        if force or not self.items:
            self.__iterate()

        self._request_params = {}
        max_items = self.maximum_items
        self.maximum_items = None
        return self.items[:max_items]

    def save(self):
        """
        Save all changed objects
        :return:
        """
        items_to_save = filter(lambda x: x.changed is True, self.items)
        for item in items_to_save:
            item.save()

    def delete(self):
        """
        Delete all objects
        :return:
        """
        for item in self.items:
            item.delete()

    def new(self):
        """
        Create new object
        :return: new object
        """
        return Entity(name=self._entity, entry_point=self, help_fields=self.help_fields)

    def clear_cache(self):
        """
        Drop cached objects
        :return:
        """
        self.items = SlicableOrderedDict()

    def options(self):
        """
        Get options from server. This method performed automatically
        :return: help_fields
        """
        text = self._api.get_response(requests.options, path=self._path).text
        try:
            actions = json.loads(text).get('actions')
        except JSONDecodeError:
            actions = None
        if actions:
            self.help_fields = actions['POST']

        return self.help_fields

    def __iter__(self):
        items = self.list()
        iterator = iter(self.items.__iter__())
        while True:
            if len(items) != len(self.items):
                iterator = iter(self.items[len(items):].__iter__())
                items = self.items
            if len(self.items) == self.count == self.maximum_items:
                break
            try:
                yield next(iterator)
            except StopIteration:
                break

    def __str__(self):
        return self._name

    __repr__ = __str__
